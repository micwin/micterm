##### Using CURL
```
   curl -sSL https://gitlab.com/micwin/micterm/-/raw/main/micterm-setup.sh | bash
```

##### Using wget
```
   wget -O - https://gitlab.com/micwin/micterm/-/raw/main/micterm-setup.sh | bash
```

##### Manual Download and Install

- Download setup by clicking [here](https://gitlab.com/micwin/micterm/-/raw/main/micterm-setup.sh?inline=false)

- give it execution rights and execute
 
```bash
   chmod +x micterm-setup.sh
   micterm-setup.sh
```

- follow steps