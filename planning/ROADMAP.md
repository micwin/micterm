Based on the identified use cases and your perspective, here's a proposed roadmap for micterm development. This roadmap outlines a phased approach, prioritizing initial setup and ease of transition between environments.

### Phase 1: Initial Setup and Configuration Script

- **Objective**: Develop a script that can be downloaded and executed to set up micterm on a client's workstation.
- **Key Actions**:
  - Create a lightweight, secure script for initial download and execution.
  - Implement a configuration system that differentiates between customer-specific and micterm-generic settings.
  - Store configurations in `$HOME/.config/micterm` on Linux-based systems.
  - Focus on bash script compatibility, particularly for Git Bash environments.
- **Expected Outcome**: A seamless initial setup process that establishes a basic, yet functional micterm environment.

### Phase 2: Cross-Environment Consistency and Secure Machine Transition

- **Objective**: Enable easy transitioning between machines while maintaining a consistent shell environment.
- **Key Actions**:
  - Develop a custom script facilitating secure SSH transitions between machines.
  - Implement pre- and post-transition checks to ensure environment consistency and security.
  - Introduce a configuration key to mark specific systems and users as "view-only," preventing any modifications on these machines.
- **Expected Outcome**: Users can move across different machines via SSH while retaining their familiar micterm environment and ensuring that no unintended changes are made to sensitive systems.
