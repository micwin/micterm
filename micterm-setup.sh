#!/bin/bash

# micterm Initial Setup Script
MICTERM_CONFIG_DIR="$HOME/.config/micterm"
MICTERM_USER_CONFIG="$MICTERM_CONFIG_DIR/user-config.txt"
MICTERM_CUSTOMER_CONFIG="$MICTERM_CONFIG_DIR/customer-config.txt"
MICTERM_REPO_DIR="$MICTERM_CONFIG_DIR/repo"
SCRIPT_DIR=$(readlink -f $(dirname $0))
MICTERM_REPO_ZIP_URL="https://gitlab.com/micwin/micterm/-/archive/main/micterm-main.tar.bz2"
MICTERM_ARCHIVE_NAME="$(basename $MICTERM_REPO_ZIP_URL)"

# Function to check for dependencies
check_dependencies() {
    command -v git >/dev/null 2>&1 || { echo >&2 "Git is required but not installed. Aborting."; exit 1; }

    # Check for curl or wget for downloading
    if command -v curl >/dev/null 2>&1; then
        readonly DOWNLOAD_CMD="curl -o"
    elif command -v wget >/dev/null 2>&1; then
        readonly DOWNLOAD_CMD="wget -O"
    else
        echo >&2 "Neither curl nor wget is available for downloading. Aborting."
        exit 1
    fi


}

# Function to configure proxy for curl and wget
set_proxy() {
    local proxies=()
    local choice

    # Check environment variables
    [[ ! -z "$http_proxy" ]] && proxies+=("http_proxy: $http_proxy")
    [[ ! -z "$https_proxy" ]] && proxies+=("https_proxy: $https_proxy")

    # Check curl configuration
    if command -v curl >/dev/null 2>&1; then
        local curl_proxy=$(grep 'proxy' "$HOME/.curlrc" | cut -d' ' -f3)
        [[ ! -z "$curl_proxy" ]] && proxies+=("curl_proxy: $curl_proxy")
    fi

    # Check wget configuration
    if command -v wget >/dev/null 2>&1; then
        local wget_http_proxy=$(grep 'http_proxy' "$HOME/.wgetrc" | cut -d' ' -f3)
        local wget_https_proxy=$(grep 'https_proxy' "$HOME/.wgetrc" | cut -d' ' -f3)
        [[ ! -z "$wget_http_proxy" ]] && proxies+=("wget_http_proxy: $wget_http_proxy")
        [[ ! -z "$wget_https_proxy" ]] && proxies+=("wget_https_proxy: $wget_https_proxy")
    fi

    # Check git configuration
    local git_http_proxy=$(git config --global http.proxy)
    local git_https_proxy=$(git config --global https.proxy)
    [[ ! -z "$git_http_proxy" ]] && proxies+=("git_http_proxy: $git_http_proxy")
    [[ ! -z "$git_https_proxy" ]] && proxies+=("git_https_proxy: $git_https_proxy")

    # Remove duplicates
    readarray -t proxies < <(printf '%s\n' "${proxies[@]}" | sort -u)

    # If multiple distinct proxies are found, ask the user to choose one
    if [ ${#proxies[@]} -gt 1 ]; then
        echo "Multiple proxy configurations detected. Please choose one:"
        for i in "${!proxies[@]}"; do
            echo "$((i+1))) ${proxies[i]}"
        done
        read -p "Enter your choice (1-${#proxies[@]}): " choice
        choice=$((choice-1))

        # Extract only the proxy URL from the choice
        local selected_proxy=$(echo "${proxies[choice]}" | cut -d' ' -f2)
        
        # Set the selected proxy for environment, curl, wget, and git
        export http_proxy="$selected_proxy"
        export https_proxy="$selected_proxy"
        echo "proxy = $selected_proxy" > "$HOME/.curlrc"
        echo "http_proxy = $selected_proxy" > "$HOME/.wgetrc"
        echo "https_proxy = $selected_proxy" >> "$HOME/.wgetrc"
        git config --global http.proxy "$selected_proxy"
        git config --global https.proxy "$selected_proxy"
        echo proxy = "$selected_proxy"
    elif [ ${#proxies[@]} -eq 1 ]; then
        # If only one proxy setting is found, use it
        local selected_proxy=$(echo "${proxies[0]}" | cut -d' ' -f2)
        export http_proxy="$selected_proxy"
        export https_proxy="$selected_proxy"
        echo proxy = "$selected_proxy"
    else
        echo "No proxy configuration detected."
    fi
    
}

# Function to clone micterm repository
clone_micterm_repo() {
    echo "Cloning micterm repository into $MICTERM_REPO_DIR..."
    mkdir -p "$MICTERM_REPO_DIR"
    git clone "https://gitlab.com/micwin/micterm.git" "$MICTERM_REPO_DIR"
    echo "Cloning completed."
}

# Function to test download capability
test_download() {
    local test_file="$TMP/test_download"
    echo "Testing download capability..."
    $DOWNLOAD_CMD "$test_file" "https://www.example.com/"

    # Check if download was successful
    if [[ ! -f "$test_file" ]]; then
        echo "No internet connectivity or proxy misconfiguration detected."
        handle_offline_mode
    else
        echo "Download test successful."
        rm -f "$test_file"  # Clean up test file
        clone_micterm_repo
    fi
}

# Function to handle offline mode setup
handle_offline_mode() {
    echo "Please download the micterm repository archive from:"
    echo "$MICTERM_REPO_ZIP_URL"
    echo "Save it as $MICTERM_ARCHIVE_NAME in the directory $SCRIPT_DIR"
    read -p "Once downloaded, type 'yes' to continue: " user_confirmation
    if [[ $user_confirmation != "yes" ]]; then
        echo "Script execution terminated due to missing connectivity."
        exit 1
    fi
    check_and_extract_archive
}

# Function to check and extract the archive
check_and_extract_archive() {
    local archive_path="$SCRIPT_DIR/$MICTERM_ARCHIVE_NAME"
    if [[ -f "$archive_path" && -s "$archive_path" ]]; then
        echo "Extracting $MICTERM_ARCHIVE_NAME into $MICTERM_REPO_DIR..."
        mkdir -p "$MICTERM_REPO_DIR"
        tar -xjf "$archive_path" --strip-components=1 -C "$MICTERM_REPO_DIR"
        echo "Extraction completed."
    else
        echo "The file $archive_path does not exist or is empty in $SCRIPT_DIR."
        echo "Please download the file and run the script again."
        exit 1
    fi
}



# Function to set up micterm
setup_micterm() {
    mkdir -p "${MICTERM_CONFIG_DIR}" && echo "Configuration directory created at ${MICTERM_CONFIG_DIR}"

    [[ -f "$MICTERM_USER_CONFIG" ]] && echo "Sourcing $MICTERM_USER_CONFIG ..." && source "$MICTERM_USER_CONFIG"
    [[ -f "$MICTERM_CUSTOMER_CONFIG" ]] && echo "Sourcing $MICTERM_CUSTOMER_CONFIG ..." && source "$MICTERM_CUSTOMER_CONFIG"

    local micterm_scripts_dir="$MICTERM_CONFIG_DIR/scripts"

    # Determine the shell and corresponding config file
    local shell_config_file
    case "$SHELL" in
        */bash)
            shell_config_file="$HOME/.bashrc"
            ;;
        */zsh)
            shell_config_file="$HOME/.zshrc"
            ;;
        # Add additional shells and their config files here if needed
        *)
            echo "Unsupported shell: $SHELL"
            return 1
            ;;
    esac

    # Add to PATH for current session
    if [[ -d "$micterm_scripts_dir" && ":$PATH:" != *":$micterm_scripts_dir:"* ]]; then
        echo "Adding micterm scripts to PATH for current session..."
        export PATH="$micterm_scripts_dir:$PATH"
    fi

    # Check and update shell configuration file for persistent PATH update
    if grep -q "$micterm_scripts_dir" "$shell_config_file"; then
        echo "micterm scripts directory is already in the PATH in $shell_config_file."
    else
        echo "Adding micterm scripts directory to PATH in $shell_config_file for future sessions..."
        {
            echo ""
            echo "# Add micterm scripts to PATH"
            echo "if [[ -d \"$micterm_scripts_dir\" && \":\$PATH:\" != *\":$micterm_scripts_dir:\"* ]]; then"
            echo "    export PATH=\"$micterm_scripts_dir:\$PATH\""
            echo "fi"
        } >> "$shell_config_file"
    fi
}
    # Source user and customer config in the shell config file
    if ! grep -q "$MICTERM_USER_CONFIG" "$shell_config_file"; then
        echo "Sourcing $MICTERM_USER_CONFIG in $shell_config_file for future sessions..."
        echo "[[ -f \"$MICTERM_USER_CONFIG\" ]] && source \"$MICTERM_USER_CONFIG\"" >> "$shell_config_file"
    fi

    if ! grep -q "$MICTERM_CUSTOMER_CONFIG" "$shell_config_file"; then
        echo "Sourcing $MICTERM_CUSTOMER_CONFIG in $shell_config_file for future sessions..."
        echo "[[ -f \"$MICTERM_CUSTOMER_CONFIG\" ]] && source \"$MICTERM_CUSTOMER_CONFIG\"" >> "$shell_config_file"
    fi



# Function to copy template and open in editor
edit_user_config() {
    local template_file="$MICTERM_CONFIG_DIR/repo/templates/config/user-config.txt"
    local target_file="$MICTERM_CONFIG_DIR/user-config.txt"

    # if config is missing, copy the template file to the config directory
    if [[ ! -f "$target_file" ]]; then
        cp "$template_file" "$target_file"
        echo "User configuration template copied to $target_file"
    else
        echo "user config already present - not overwriting"
    fi

    # Determine which editor to use
    local editor="${EDITOR:-vim}"

    # Open the user config file in the editor
    echo "Opening $target_file in $editor..."
    $editor "$target_file"

    source $target_file
}


# Main execution
setup_micterm
check_dependencies
set_proxy
test_download
edit_user_config

# Propose to continue with setup-git.sh
echo "micterm setup completed successfully"
